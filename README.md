# Travaux Pratique Application Répartie
> authors : Brun Samuel, Lithaud Alexandre
---

## How to launch the final HTTP Server :

If you want to run the program smoothly run the [step 4 HttpServer.java main method](./TP-AR/src/serverHTTP/step4/impl/httpserver/itf/impl/HttpServer.java) using this configuration : `[N° PORT] src/serverHTTP/` 

---
- [Travaux Pratique Application Répartie](#travaux-pratique-application-répartie)
  - [How to launch the final HTTP Server :](#how-to-launch-the-final-http-server-)
- [TP 1 - Socket : COURS | TP](#tp-1---socket--cours--tp)
  - [Baby step : SRC](#baby-step--src)
  - [Basic file server : SINGLE\_QUERY | MULTI\_QUERY](#basic-file-server--single_query--multi_query)
  - [**Multi-threaded file server**](#multi-threaded-file-server)
    - [Basic Multi-threaded Design : SINGLE\_QUERY | MULTI\_QUERY](#basic-multi-threaded-design--single_query--multi_query)
    - [Pool-based Multi-threaded Design : SINGLE\_QUERY | MULTI\_QUERY](#pool-based-multi-threaded-design--single_query--multi_query)
  - [**Considering large files**](#considering-large-files)
    - [Performances : SINGLE\_QUERY | MULTI\_QUERY](#performances--single_query--multi_query)
    - [Failures : TODO](#failures--todo)
  - [Performance evaluation : SRC](#performance-evaluation--src)
  - [Lessons learned and open questions](#lessons-learned-and-open-questions)
- [TP 2 - Java/NIO : COURS PART 1 | COURS PART 2 | TP](#tp-2---javanio--cours-part-1--cours-part-2--tp)
  - [Baby Step 1 : SRC](#baby-step-1--src)
  - [Baby Step 2 : SRC](#baby-step-2--src)
  - [Baby Step 3 : SRC](#baby-step-3--src)
  - [The real Thing : SRC](#the-real-thing--src)
- [TP 3 - Java/RMI : COURS | TP](#tp-3---javarmi--cours--tp)
  - [Baby Step : SRC](#baby-step--src-1)
  - [Chat Server Version 1 : SRC](#chat-server-version-1--src)
  - [Chat Server Version 2 : SRC](#chat-server-version-2--src)
  - [Chat Server Version 3 : SRC](#chat-server-version-3--src)
- [TP 4 - Apache : COURS | TP](#tp-4---apache--cours--tp)
- [TP 5 - "My own HTTP Server" : PRESENTATION | CLASS LOADER | TP](#tp-5---my-own-http-server--presentation--class-loader--tp)
  - [STEP 1 - Serving static pages : SRC](#step-1---serving-static-pages--src)
  - [STEP 2 - Dynamic pages : SRC](#step-2---dynamic-pages--src)
  - [STEP 3 - Introducing Cookies : SRC](#step-3---introducing-cookies--src)
  - [STEP 4 - Session Management : SRC](#step-4---session-management--src)
  - [STEP 5 - Application Management \[OPTIONAL\] : TODO](#step-5---application-management-optional--todo)


---

# TP 1 - Socket : [COURS](./Cours/Sockets.pdf) | [TP](./TP/Projet-Sockets.pdf)

## Baby step : [SRC](./TP-AR/src/tp1_Socket/baby_step/)

## Basic file server : [SINGLE_QUERY](./TP-AR/src/tp1_Socket/basic_file_server/single_query/) | [MULTI_QUERY](./TP-AR/src/tp1_Socket/basic_file_server/multiple_query/)

## **Multi-threaded file server**

### Basic Multi-threaded Design : [SINGLE_QUERY](./TP-AR/src/tp1_Socket/multiT_basic_file_server/single_query/) | [MULTI_QUERY](./TP-AR/src/tp1_Socket/multiT_basic_file_server/multiple_query/)


### Pool-based Multi-threaded Design : [SINGLE_QUERY](./TP-AR/src/tp1_Socket/multiT_pool_based_file_server/single_query/) | [MULTI_QUERY](./TP-AR/src/tp1_Socket/multiT_pool_based_file_server/multiple_query/)

## **Considering large files**

### Performances : [SINGLE_QUERY](./TP-AR/src/tp1_Socket/large_file_server/single_query/) | [MULTI_QUERY](./TP-AR/src/tp1_Socket/large_file_server/multiple_query/)

### Failures : [TODO](.)

## Performance evaluation : [SRC](./Performance_Evaluation.sh)

## Lessons learned and open questions

---
# TP 2 - Java/NIO : [COURS PART 1](./Cours/NIO.pdf) | [COURS PART 2](./Cours/NIO-part2.pdf) | [TP](./TP/TP-NIO.pdf)

## Baby Step 1 : [SRC](./TP-AR/src/tp2_NIO/babystep1/)

## Baby Step 2 : [SRC](./TP-AR/src/tp2_NIO/babystep2/)

## Baby Step 3 : [SRC](./TP-AR/src/tp2_NIO/babystep3/)

## The real Thing : [SRC](./TP-AR/src/tp2_NIO/real_thing/ricm/channels/)
---
# TP 3 - Java/RMI : [COURS](./Cours/RMI.pdf) | [TP](./TP/TP-RMI.pdf)

## Baby Step : [SRC](./TP-AR/src/tp3_RMI/baby_step/)

## Chat Server Version 1 : [SRC](./TP-AR/src/tp3_RMI/chat_server_V1/)

## Chat Server Version 2 : [SRC](./TP-AR/src/tp3_RMI/chat_server_V2/)

## Chat Server Version 3 : [SRC](./TP-AR/src/tp3_RMI/chat_server_V3/)

---
# TP 4 - Apache : [COURS](./Cours/Servlets.pdf) | [TP](./TP/TP-APACHE.pdf)
---
# TP 5 - "My own HTTP Server" : [PRESENTATION](./Cours/PresentationProject.pdf) | [CLASS LOADER](./Cours/Lecture-Web-Class-Loader.pdf) | [TP](./TP/TP-WEB.pdf)

## STEP 1 - Serving static pages : [SRC](./TP-AR/src/serverHTTP/step1/)

## STEP 2 - Dynamic pages : [SRC](./TP-AR/src/serverHTTP/step2/)

## STEP 3 - Introducing Cookies : [SRC](./TP-AR/src/serverHTTP/step3/)

## STEP 4 - Session Management : [SRC](./TP-AR/src/serverHTTP/step4/)

## STEP 5 - Application Management [OPTIONAL] : [TODO](.)

---
