package tp3_RMI.chat_server_V2.server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import tp3_RMI.chat_server_V2.common.IChatRoom;

public class Server {
	static int PORT = 1099;
	
	public static void main(String[] args) throws RemoteException {
		IChatRoom room = new ChatRoom();
		
		Registry reg = LocateRegistry.createRegistry(PORT);
		reg.rebind("ChatRoom", room);
		
	}
}
