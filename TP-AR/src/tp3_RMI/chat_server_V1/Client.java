package tp3_RMI.chat_server_V1;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

public class Client {
	static int PORT = 1099;
	static String host = "localhost";
	public static void main(String[] args) throws RemoteException, NotBoundException {
		
		Registry reg = LocateRegistry.getRegistry(host, PORT);
		IChatRoom room = (IChatRoom) reg.lookup("ChatRoom");
		
		IParticipant participant = new Participant(args[0]);
		
		room.connect(participant);
		
		Scanner s = new Scanner(System.in);

		String rep;
		while(true) {
			rep = s.nextLine();
			if(rep.split(" ",1)[0].equals("quit")) {
				room.leave(participant);
				break;
			}
			else if(rep.split(" ",1)[0].equals("who")){
				String[] res = room.who();
				for(String n:res) {
					System.out.println(n);
				}
			}
			else {
				System.out.println("pass");
				room.send(participant, rep);
			}
				
		}
		System.out.println("That's all folks");
	}
}
