package tp3_RMI.chat_server_V1;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class ChatRoom  extends UnicastRemoteObject implements IChatRoom{
	private ArrayList<IParticipant> partList = new ArrayList<>();
	private String name;

	protected ChatRoom() throws RemoteException {
		super();
		this.name = "TestRoom";
	}

	@Override
	public String name() throws RemoteException {
		return name;
	}

	@Override
	public void connect(IParticipant p) throws RemoteException {
		partList.add(p);
	}

	@Override
	public void leave(IParticipant p) throws RemoteException {
		partList.remove(p);
	}

	@Override
	public String[] who() throws RemoteException {
		String[] res = new String[partList.size()];
		int i=0;
		for(IParticipant p: partList) {
			res[i] = p.name();
			i++;
		}
		return res;
	}

	@Override
	public void send(IParticipant p, String msg) throws RemoteException {
		for(IParticipant part : partList) {
			if(!p.equals(part)) {
				part.receive(p.name(), msg);
			}
		}
	}

}
