package tp3_RMI.chat_server_V1;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {
	static int PORT = 1099;
	
	public static void main(String[] args) throws RemoteException {
		IChatRoom room = new ChatRoom();
		
		Registry reg = LocateRegistry.createRegistry(PORT);
		reg.rebind("ChatRoom", room);
		
	}
}
