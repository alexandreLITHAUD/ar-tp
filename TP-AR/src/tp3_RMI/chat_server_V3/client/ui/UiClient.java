package tp3_RMI.chat_server_V3.client.ui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import tp3_RMI.chat_server_V3.client.Participant;
import tp3_RMI.chat_server_V3.common.IChatHub;
import tp3_RMI.chat_server_V3.common.IChatRoom;
import tp3_RMI.chat_server_V3.common.IParticipant;


public class UiClient extends JFrame {
	private enum State {
		InRoom, InHub
	};

	private State state = State.InHub;

	private JSplitPane splitPane;
	private static String partName;
	private IParticipant part;

	private IChatRoom currentRoom;
	private String currentRoomName;
	private UiMiddlePane currentUiModel;

	private JPanel pane;
	
	private JList roomList;
	private JButton addButton;
	private JButton joinButton;
	
	private Map<String, ArrayList<String>> listeMessagesTmp = new HashMap<>();

	private JComponent getMiddlePane() {
		if (state == State.InHub) {
			return pane;
		} else {
			return currentUiModel;
		}
	}

	public void toRoomState(IChatRoom r, String n) {
		if (state == State.InHub) {
			state = State.InRoom;
			currentRoom = r;
			currentRoomName = n;
			currentUiModel = new UiMiddlePane(this, part, r, n);
			splitPane.remove(pane);
			splitPane.setRightComponent(currentUiModel);
			addButton.setEnabled(false);
			joinButton.setEnabled(false);
		} else {
			System.out.println("should never append !");
		}
	}

	public void toHubState() {
		if (state == State.InRoom) {
			state = State.InHub;

			splitPane.remove(currentUiModel);
			splitPane.setRightComponent(pane);
			currentRoom = null;
			currentRoomName = null;
			currentUiModel = null;
			addButton.setEnabled(true);
			joinButton.setEnabled(true);
		} else {
			System.out.println("should never append !");
		}
	}

	public void receive(String name, String msg) {
		if (state == State.InRoom) {
			currentUiModel.receive(name, msg);
		} else {
			ArrayList<String> l = listeMessagesTmp.get(name);
			if(l==null) {
				l = new ArrayList<String>();
				l.add(msg);
				listeMessagesTmp.put(name,l);
			}
			else {
				l.add(msg);
			}
			
		}
	}

	UiClient(IChatHub hub, String title, String[] args) {
		super(title);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(1000, 800);
		try {
			this.part = new Participant(partName, this);
		} catch (RemoteException e1) {
		}

		// LEFT PANEL
		DefaultListModel<String> roomsList = new DefaultListModel<>();
		roomList = new JList(roomsList);
		Action aAdd = new AbstractAction("Create") {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFrame jf = new JFrame();
				JOptionPane jop = new JOptionPane();
				String wantedName = "";

				while (true) {
					wantedName = jop.showInputDialog(jf, "Choose a room name :", "NamePanel",
							JOptionPane.QUESTION_MESSAGE);

					if (isNameValid(wantedName)) {
						break;
					} else {
						JOptionPane jopErr = new JOptionPane();
						jopErr.showMessageDialog(jf, "Choose a real name !", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}

				try {
					hub.createRoom(wantedName);
					roomsList.addElement(wantedName);
					//toRoomState(r, wantedName);
				} catch (Exception e) {
					JOptionPane jopErr = new JOptionPane();
					jopErr.showMessageDialog(jf, "Error during creation process !", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		};
		addButton = new JButton(aAdd);
		Action joinRoomAct = new AbstractAction("Join") {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					String s = (String) roomList.getSelectedValue();
					if(s != null) {
						IChatRoom r = hub.joinRoom(s,part);
						toRoomState(r, s);
						for (Entry<String, ArrayList<String>> entry : listeMessagesTmp.entrySet()) {
							ArrayList<String> l = entry.getValue();
							for(String msg:l) {
								currentUiModel.receive(entry.getKey(), msg);
							}
						}
						listeMessagesTmp.clear();
					}
				} catch (Exception e) {
					JPanel jf = new JPanel();
					JOptionPane jopErr = new JOptionPane();
					jopErr.showMessageDialog(jf, "Error during joining process !", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		};
		joinButton = new JButton(joinRoomAct);
		JPanel addJoin = new JPanel(new GridLayout(1, 2));
		addJoin.add(addButton);
		addJoin.add(joinButton);

		JLabel l = new JLabel("Liste des salles");
		JPanel leftPane = new JPanel(new BorderLayout());
		leftPane.add(roomList, BorderLayout.CENTER);
		leftPane.add(addJoin, BorderLayout.SOUTH);
		leftPane.add(l, BorderLayout.NORTH);

		// MIDDLE
		pane = new JPanel();

		// BOTH MIDDLE LEFT
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, (Component) leftPane, (Component) getMiddlePane());
		splitPane.setResizeWeight(0.1);
		splitPane.setOneTouchExpandable(true);
		splitPane.setDividerLocation(150);

		// BAR DES MENUS
		/*
		JMenuBar jmb = new JMenuBar();
		JMenu menu = new JMenu("Menu1");
		JMenuItem addValue = new JMenuItem();
		addValue.setAction(aAdd);
		addValue.setAccelerator(KeyStroke.getKeyStroke('N'));// KeyStroke.getKeyStroke('N',CTRL_DOWN_MASK));
		menu.add(addValue);
		jmb.add(menu);*/

		// ADD
		//this.setJMenuBar(jmb);
		this.add(splitPane);
	}

	private static boolean isNameValid(String name) {
		return name != null && name.length() != 0;
	}

	public static void initUi(IChatHub hub, String[] argv) {
		JFrame jf = new JFrame();
		JOptionPane jop = new JOptionPane();
		String wantedClientName = null;
		while (true) {
			wantedClientName = jop.showInputDialog(jf, "Choose your name :", "NamePanel", JOptionPane.QUESTION_MESSAGE);

			if (isNameValid(wantedClientName)) {
				break;
			} else {
				JOptionPane jopErr = new JOptionPane();
				jopErr.showMessageDialog(jf, "Choose a real name !", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}

		UiClient.partName = wantedClientName;

		System.setProperty("apple.laf.useScreenMenuBar", "true");
		final String[] expressions = argv;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new UiClient(hub, "Chat", expressions).setVisible(true);
			}
		});

	}

}
