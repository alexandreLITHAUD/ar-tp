package tp3_RMI.chat_server_V3.client.ui;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.rmi.RemoteException;

import tp3_RMI.chat_server_V3.common.IChatRoom;
import tp3_RMI.chat_server_V3.common.IParticipant;

public class UiMiddlePane extends JSplitPane{
	private IChatRoom room;
	private String roomName;
	private IParticipant part;
	private UiClient clientUi;
	private JTextArea middlePaneText;
	private JScrollPane middlePane;
	private DefaultListModel<String> partListModel;
	
	private void leaveRoom() throws RemoteException {
		room.leave(part);
		clientUi.toHubState();
	}
	
	public UiMiddlePane(UiClient ui,IParticipant p, IChatRoom roomI,String name) {
		this.room = roomI;
		roomName = name;
		part = p;
		clientUi = ui;
		
		JPanel panel = new JPanel(new BorderLayout());
		
		//NORD
		JPanel panelNord = new JPanel(new BorderLayout());
		JLabel nameNord = new JLabel("Room Name : "+name);
		Action quit = new AbstractAction("Quit room") {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					leaveRoom();
				} catch (RemoteException e) {
					JPanel jf = new JPanel();
					JOptionPane jopErr = new JOptionPane();
					jopErr.showMessageDialog(jf, "Error during leaving process !", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		};
		JButton quitButton = new JButton(quit);
		panelNord.add(nameNord,BorderLayout.CENTER);
		panelNord.add(quitButton,BorderLayout.EAST);
		panel.add(panelNord,BorderLayout.NORTH);
		
		//SUD
		JPanel panelSud = new JPanel(new BorderLayout());
		JTextField message = new JTextField("");
		Action send = new AbstractAction("send") {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					if(!message.equals("")) {
						room.send(p, message.getText());
						message.setText("");
					}
				} catch (Exception e) {
					JPanel jf = new JPanel();
					JOptionPane jopErr = new JOptionPane();
					jopErr.showMessageDialog(jf, "Error during sending process !", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		};
		JButton sendButton = new JButton(send);
		panelSud.add(message,BorderLayout.CENTER);
		panelSud.add(sendButton,BorderLayout.EAST);
		panel.add(panelSud,BorderLayout.SOUTH);
		
		
		//Middle
		middlePaneText = new JTextArea("Ici s'affiche les messages de la room\n");
		middlePaneText.setEditable(false);
		middlePane = new JScrollPane(middlePaneText);
		//middlePane.add(middlePaneText);
		panel.add(middlePane,BorderLayout.CENTER);
		
		//SECOND PART OF SPLIT PANE
		partListModel = new DefaultListModel<>();
		String[] whoRes = null;
		try {
			whoRes = room.who();
		} catch (Exception e) {
		}

		for (String n : whoRes) {
			partListModel.addElement(n);
		}
		JList partList = new JList(partListModel);
		panel.add(partList, BorderLayout.EAST);
		
		
		//palcement des composant
		setLeftComponent(panel);
		setRightComponent(partList);
	}
	
	public void receive(String name,String msg) {
		middlePaneText.append(name + " > " +msg + "\n");
		//check des présents
		partListModel.removeAllElements();
		String[] whoRes = null;
		try {
			whoRes = room.who();
		} catch (Exception e) {
		}

		for (String n : whoRes) {
			partListModel.addElement(n);
		}
		revalidate();
	}
	
}
