package tp3_RMI.chat_server_V3.client;

import java.io.FileInputStream;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Properties;
import java.util.Scanner;

import tp3_RMI.chat_server_V3.common.IChatRoom;
import tp3_RMI.chat_server_V3.common.IParticipant;
import tp3_RMI.chat_server_V3.client.ui.UiClient;
import tp3_RMI.chat_server_V3.common.IChatHub;

public class Client {
	
	public static Properties propFile;
	private static String PATH_TO_OPTION_FILE = "src/tp3_RMI/chat_server_V3/client/";
	

	public static void main(String[] args) throws RemoteException, NotBoundException {
		propFile = new Properties();
		try {
			propFile.loadFromXML(new FileInputStream(PATH_TO_OPTION_FILE+"options.xml")); 
		}catch(IOException e) {
			System.err.println("Errors in Option File : Defaults values will be set");
		} 

		//get default values
		String name = propFile.getProperty("defaultName");
		int PORT = Integer.parseInt(propFile.getProperty("serverPort"));
		String host = propFile.getProperty("serverIP");
		String mode = propFile.getProperty("interfaceMode");
		
		//register
		Registry reg = LocateRegistry.getRegistry(host, PORT);
		IChatHub hub = (IChatHub) reg.lookup("ChatHub");
		
		IParticipant participant = null;
		
		//Graphical
		if(mode.equals("Graphique")) {
			UiClient.initUi(hub, args);
			return;
		}
		
		
		
		// Textuel case
		if(args.length == 1) {
			name = args[0];
		}
		
		
		participant = new Participant(name);

		IChatRoom room;

		System.out.println("Debut du client : "+name);
		
		Scanner s = new Scanner(System.in);
		
		String rep;
		
		while (true) {
			rep = s.nextLine();
			if (rep.split(" ", 1)[0].equals("quit")) {
				// prevenir hub du quit ? pas la peine
				System.out.println("Vous quittez le hub !");
				break;
			} else if(rep.split(" ", 2)[0].equals("join")){
				String roomName = rep.split(" ", 2)[1];
				room = hub.joinRoom(roomName, participant);
				System.out.println("Début de la communiaction avec : " + roomName);
				roomComunication(s, room, participant);
			} else if(rep.split(" ", 1)[0].equals("get")){
				System.out.println("Rooms dispo :");
				String[] roomList = hub.getAllRoom();
				if(roomList.length != 0) {
					for(String str:roomList) {
						System.out.println(str);
					}
				}else {
					System.out.println("Aucune room !");
				}
			} else {
				System.out.println("Commande inconnue : \n\t join <roomName> \n\t get \n\t quit");
			}

		}
		System.out.println("That's all folks");
	}

	public static void roomComunication(Scanner s, IChatRoom room, IParticipant participant) throws RemoteException {
		
		String rep = "";
		while (true) {
			rep = s.nextLine();
			if (rep.split(" ", 1)[0].equals("quit")) {
				room.leave(participant);
				System.out.println("Fin de la communication avec la chatRoom");
				break;
			} else if (rep.split(" ", 1)[0].equals("who")) {
				String[] res = room.who();
				for (String n : res) {
					System.out.println(n);
				}
			} else {
				room.send(participant, rep);
			}

		}

	}
}
