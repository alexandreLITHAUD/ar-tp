package tp3_RMI.chat_server_V3.client;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import tp3_RMI.chat_server_V3.client.ui.UiClient;
import tp3_RMI.chat_server_V3.client.ui.UiMiddlePane;
import tp3_RMI.chat_server_V3.common.IParticipant;

public class Participant extends UnicastRemoteObject implements IParticipant{
	private String name;
	private UiClient ui;
	
	public Participant(String name) throws RemoteException {
		super();
		this.name = name;
		this.ui = null;
	}
	
	public Participant(String name,UiClient ui) throws RemoteException {
		super();
		this.name = name;
		this.ui = ui;
	}

	@Override
	public String name() throws RemoteException {
		return name;
	}

	@Override
	public void receive(String name, String msg) throws RemoteException {
		if(ui != null) {
			ui.receive(name,msg);
		}else {
			System.out.println(name + " > " + msg);
		}
	}

}
