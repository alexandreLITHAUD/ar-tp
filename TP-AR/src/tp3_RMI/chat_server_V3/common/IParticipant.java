package tp3_RMI.chat_server_V3.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IParticipant extends Remote  {
	public String name() throws RemoteException;
	public void receive(String name, String msg) throws RemoteException;
}
