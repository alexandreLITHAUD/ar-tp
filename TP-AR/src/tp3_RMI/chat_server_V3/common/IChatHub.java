package tp3_RMI.chat_server_V3.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IChatHub extends Remote {
	
	public String[] getAllRoom() throws RemoteException;
	public IChatRoom joinRoom(String name, IParticipant p) throws RemoteException;
	public void createRoom(String name) throws RemoteException;
	public void removeRoom(String name) throws RemoteException;

}
