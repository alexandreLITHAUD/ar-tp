package tp3_RMI.chat_server_V3.server;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Map;

import tp3_RMI.chat_server_V3.common.IParticipant;

public class MockParticipant implements IParticipant{

	private ArrayList<Pair<IParticipant, String>> listeMessage = new ArrayList<>();
	private String name;

	public MockParticipant(String name){
		this.name = name;
	}


	@Override
	public String name() throws RemoteException {
		// TODO Auto-generated method stub
		return this.name;
	}

	public void receive(String name, String msg) throws RemoteException {
		// TODO Auto-generated method stub
		
	}
	
	public void addMessage(IParticipant p, String message){
		
		Pair<IParticipant, String> pair = new Pair<IParticipant, String>(p, message);
		this.listeMessage.add(pair);
		
	}
	
	public ArrayList<Pair<IParticipant, String>> getMessageList(){
		return this.listeMessage;
	}

}
