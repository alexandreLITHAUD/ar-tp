package tp3_RMI.chat_server_V3.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import tp3_RMI.chat_server_V3.common.IChatHub;
import tp3_RMI.chat_server_V3.common.IChatRoom;
import tp3_RMI.chat_server_V3.common.IParticipant;

public class ChatHub extends UnicastRemoteObject implements IChatHub{
	protected Map<String, IChatRoom> roomMap = new HashMap<>();

	protected ChatHub() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public synchronized String[] getAllRoom() throws RemoteException {
		return (String[]) roomMap.keySet().toArray(new String[roomMap.keySet().size()]);
	}

	@Override
	public synchronized IChatRoom joinRoom(String name, IParticipant p) throws RemoteException {
		// TODO Auto-generated method stub
		if(roomMap.containsKey(name)) {
			// return existing room
			IChatRoom res = roomMap.get(name);
			res.connect(p);
			return res;
		}else {
			//create new room with the name
			IChatRoom res = new ChatRoom(name,this);
			roomMap.put(name, res);
			res.connect(p);
			return res;
		}
				
	}
	
	public synchronized void removeRoom(String name){
		roomMap.remove(name);
	}

	@Override
	public synchronized void createRoom(String name) throws RemoteException {
		if(!roomMap.containsKey(name)) {
			IChatRoom res = new ChatRoom(name,this);
			roomMap.put(name, res);
		}
	}

}
