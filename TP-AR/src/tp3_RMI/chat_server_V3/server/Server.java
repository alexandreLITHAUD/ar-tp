package tp3_RMI.chat_server_V3.server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import tp3_RMI.chat_server_V3.common.IChatHub;

public class Server {
	static int PORT = 1099;
	
	public static void main(String[] args) throws RemoteException {
		IChatHub hub = new ChatHub();
		
		Registry reg = LocateRegistry.createRegistry(PORT);
		reg.rebind("ChatHub", hub);
		System.out.println("Server running.");
	}
}
