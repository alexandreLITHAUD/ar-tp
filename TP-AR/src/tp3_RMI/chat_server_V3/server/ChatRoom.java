package tp3_RMI.chat_server_V3.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import tp3_RMI.chat_server_V3.common.IChatHub;
import tp3_RMI.chat_server_V3.common.IChatRoom;
import tp3_RMI.chat_server_V3.common.IParticipant;

public class ChatRoom extends UnicastRemoteObject implements IChatRoom {
	private ArrayList<IParticipant> partList = new ArrayList<>();
	private ArrayList<MockParticipant> mockPartList = new ArrayList<>();

	private String name;
	private IChatHub hub;

	protected ChatRoom(String name, IChatHub b) throws RemoteException {
		super();
		this.name = name;
		this.hub = b;
	}

	@Override
	public String name() throws RemoteException {
		return name;
	}

	@Override
	public synchronized void connect(IParticipant p) throws RemoteException {

		try {
			String name = p.name();
			for (MockParticipant mp : mockPartList) {
				if (mp.name().equals(name)) {
					// si il y a un mock pour le participant courant p
					
					for(Pair<IParticipant, String> pair : mp.getMessageList()) {
						if(pair.getKey().name().equals(name)) {
							continue;
						}
						p.receive(pair.getKey().name(), pair.getValue());
					}

				}
				mockPartList.remove(mp);
			}
		} catch (RemoteException e) {
			// name ou receive on levé une erreur
		}
		
		partList.add(p);

	}

	@Override
	public synchronized void leave(IParticipant p) throws RemoteException {
		partList.remove(p);
		
		try {
			mockPartList.add(new MockParticipant(p.name()));
		}catch(RemoteException e) {}
		//System.out.println("Living the room");
	}

	@Override
	public synchronized String[] who() throws RemoteException {
		String[] res = new String[partList.size()];
		int i = 0;
		for (IParticipant p : partList) {
			try {
				res[i] = p.name();
			}catch(RemoteException e) {
				partList.remove(i);
				// GARDER L'ANCIEN NOM POUR CREER UN MOCK
				continue;
			}
			
			i++;
		}
		return res;
	}

	@Override
	public void send(IParticipant p, String msg) throws RemoteException {

		for (MockParticipant mock : this.mockPartList) {
			mock.addMessage(p, msg);
		}

		for (IParticipant part : partList) {
			//if (!p.equals(part)) {
				try {
					part.receive(p.name(), msg);
				} catch (RemoteException e) {

				}
			//}
		}
	}
}
