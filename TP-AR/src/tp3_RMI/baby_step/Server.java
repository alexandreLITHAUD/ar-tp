package tp3_RMI.baby_step;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {

	static int PORT = 9999;
	
	public static void main(String[] args) throws RemoteException {
		
		Remote printer = new Printer();
		
		Registry reg = LocateRegistry.createRegistry(PORT);
		
		reg.rebind("printer", printer);
		
	}
}
