package tp3_RMI.baby_step;

import java.rmi.NotBoundException;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client {

	public static void main(String[] args) throws RemoteException, NotBoundException {
		
		Registry reg = LocateRegistry.getRegistry("127.0.0.1", 9999);
		
		IPrinter printer = (IPrinter) reg.lookup("printer");
		
		printer.print("THAT'S ALL FOLKS");
		
	}

}
