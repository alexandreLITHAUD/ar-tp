package tp1_Socket.baby_step;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {

	public static void main(String[] args) throws UnknownHostException, IOException {

		Socket soc = new Socket("localhost", 9999);

		Scanner sc = new Scanner(System.in);

		String name = sc.nextLine();

		String finalS = "Hello " + name;

		DataOutputStream dos = null;

		try {
			dos = new DataOutputStream(soc.getOutputStream());
			dos.writeInt(finalS.getBytes("UTF-8").length);
			dos.write(finalS.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dos != null) {
				try {
					dos.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.println(name + " done \n");

	}

}
