package tp1_Socket.baby_step;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	
	public static void main(String[] args) throws IOException {
		
		ServerSocket serverSocket = new ServerSocket(9999);
		
		while(true) {
			
			Socket soc = serverSocket.accept();
			
			DataInputStream dis = new DataInputStream(soc.getInputStream());
			
			int length = dis.readInt();
			
			byte[] b = new byte[length];
			
			dis.readFully(b);
			
			String s = new String(b,"UTF-8");
			
			System.out.println(s+"\n");
		}
		
		
	}
	
}
