package tp1_Socket.multiT_basic_file_server.multiple_query;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	protected static String REPO_PATH;
	protected static int PORT;
	
	public static void main(String[] args) throws IOException {

		System.out.println("SERVER STARTING !");
		
		REPO_PATH = null;
		PORT = 0;
		
		if(args.length < 2) {
			System.out.println("Missing paramters\n Lanching in default mode\n");
			REPO_PATH = "ressources_serv/";
			PORT = 9999;
			System.out.println("PATH : " + REPO_PATH + "\nPORT : " + PORT + "\n");
		}
		else {
			System.out.println("Lanching in argument mode\n");
			REPO_PATH = args[0];
			PORT = Integer.valueOf(args[1]);
			System.out.println("PATH : " + REPO_PATH + "\nPORT : " + PORT + "\n");
		}

		ServerSocket sso = new ServerSocket(PORT);
		
		
		while(true) {
			
			Socket soc = sso.accept();
			
			Worker worker = new Worker(soc);
			worker.start();
		}

	}
	
	public static class Worker extends Thread{
		
		private Socket soc;
		DataInputStream dis;
		DataOutputStream dos;
		FileInputStream fis = null;
		
		public Worker(Socket s) {
			this.soc = s;
		}
		
		public void run() {
			
			try {

				dis = new DataInputStream(soc.getInputStream());
				dos = new DataOutputStream(soc.getOutputStream());
				
				while (true) {

					String str = dis.readUTF();

					
					File f = null;
					
					try {
						f = new File(REPO_PATH + str);
						fis = new FileInputStream(f);
					}catch(FileNotFoundException e) {
						System.err.println("File not found !\n");
						dos.writeInt(-50);
						continue;
					}
					
					byte[] b = fis.readAllBytes();

					dos.writeInt(b.length);

					dos.write(b);

				}
				
			} catch (Exception e) {
				System.out.println("WORKER DONE");
			} finally {
				if (dis != null) {
					try {
						dis.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if (dos != null) {
					try {
						dos.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if (fis != null) {
					try {
						fis.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				try {
					this.soc.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}

}
