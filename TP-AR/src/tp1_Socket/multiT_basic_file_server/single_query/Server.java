package tp1_Socket.multiT_basic_file_server.single_query;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.*;

public class Server {
	public final static int backlog = 3;
	
	public static void main(String[] args) {
		
		if (args.length != 2) {
			System.out.println("Use with 2 arguments : port-number(>1024) and repo-search-name(from the program root)");
			System.exit(1);
		}
		
		int port = Integer.parseInt(args[0]);
		String repo = args[1];
		
		//creation connexion
		ServerSocket listenSoc = null;
		try {
			listenSoc = new ServerSocket(port, backlog);
		}catch(Exception e) {
			System.out.println("Creation socket impossible !");
			e.printStackTrace();
			System.exit(1);
		}
		
		System.out.println("Serveur is running ...");
		while(true) {
			try {
				Socket soc = listenSoc.accept();
				Worker w = new Worker(soc,repo);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
						
		}

	}

}
