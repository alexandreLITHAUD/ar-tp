package tp1_Socket.multiT_pool_based_file_server.single_query;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;

public class Worker implements Runnable{
	private Thread th;
	private ArrayBlockingQueue<Socket> prodCons;
	private String repo;
	
	public Worker (ArrayBlockingQueue<Socket> pc,String repo) {
		th = new Thread(this);
		this.prodCons = pc;
		this.repo = repo;
		
		th.setDaemon(true);
		th.start();
	}
	
	@Override
	public void run() {
		while(true) {
			DataInputStream strIn = null;
			DataOutputStream strOut = null;
			try {		
				Socket soc = prodCons.take();
				
				strIn = new DataInputStream(soc.getInputStream());
				strOut = new DataOutputStream(soc.getOutputStream());
		
				//lecture du nom fichier 
				String fileName = strIn.readUTF();
				
				//verification existance du fichier :
				File f = new File(repo+"/"+fileName);
				if(f.exists() && !f.isDirectory()){
					byte[] buffer = new byte[512];
					long size = f.length();
					
					strOut.writeLong(size);//Code pour fichier trouvé
					
					//gestion du fichier
					FileInputStream fIn = new FileInputStream(f);
					
					//envoie du fichier par morceaux de 512o
					long i=size;
					int offset = 0;
					while(i>0) {
						int numberSend;
						//si plus de 512o restant a envoyer
						if(i>=512) {
							numberSend = 512;
						}//sinon (moins de 512 a envoyer)
						else {
							numberSend = (int) i;
						}
						
						//envoie de la taille du bloc fichier
						strOut.writeInt(numberSend);
						//lecture du bloc dans le fichier
						fIn.read(buffer, 0, numberSend);
						
						//ecriture des données 
						strOut.write(buffer, 0, numberSend);
						
						//maj offset
						offset += numberSend;
						//maj du compteur du restant a envoyer
						i -= numberSend;
					}
					
					//fermeture fichier
					fIn.close();
				}else {
					System.out.println("Fichier non trouvé "+f.getAbsolutePath());
					strOut.writeLong(-1);//Code pour fichier non trouvé
				}
				
				
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				//soc.close();
				System.out.println("Fin de client !");
			}		
		}

	}
	
	
	

}
