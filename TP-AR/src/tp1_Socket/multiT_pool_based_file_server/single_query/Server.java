package tp1_Socket.multiT_pool_based_file_server.single_query;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;

public class Server {
	public final static int backlog = 3;
	public final static int THREAD_POOL_SIZE = 5;
	public final static int PROD_CONS_SIZE = 10;
	
	public static void main(String[] args) {
		
		if (args.length != 2) {
			System.out.println("Use with 2 arguments : port-number(>1024) and repo-search-name(from the program root)");
			System.exit(1);
		}
		
		int port = Integer.parseInt(args[0]);
		String repo = args[1];
		
		//creation connexion
		ServerSocket listenSoc = null;
		try {
			listenSoc = new ServerSocket(port, backlog);
		}catch(Exception e) {
			System.out.println("Creation socket impossible !");
			e.printStackTrace();
			System.exit(1);
		}
		
		ArrayBlockingQueue<Socket> prodCons = new ArrayBlockingQueue<>(PROD_CONS_SIZE);
		ArrayList<Worker> threadList = new ArrayList<>();
		for(int i=0;i<THREAD_POOL_SIZE;i++) {
			threadList.add(new Worker(prodCons,repo));
		}
		
		System.out.println("Serveur is running ...");
		while(true) {
			Socket soc = null;
			try {
				soc = listenSoc.accept();
				prodCons.put(soc);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				try {
					soc.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}		
		}

	}

}
