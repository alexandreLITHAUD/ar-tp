package tp1_Socket.multiT_pool_based_file_server.single_query;

import java.util.Scanner;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.*;

public class Client {
	
	private final static String downloads = "ressources_cli";
	
	public static void main(String[] args) {
		
		if (args.length != 3) {
			System.out.println("Use with 3 arguments : server-hostname and server-port and file-Name");
			System.exit(1);
		}
		
		//traitement des arguments
		String serverHost = args[0];
		int port = Integer.parseInt(args[1]);
		String FileName = args[2];
		
		//creation connexion
		Socket soc=null;
		try {
			soc = new Socket(serverHost, port);
		}catch(Exception e) {
			System.out.println("Creation socket impossible !");
			e.printStackTrace();
			System.exit(1);
		}
		
		//corps
		try {
			//ES data socket
			DataInputStream strIn = new DataInputStream(soc.getInputStream());
			DataOutputStream strOut = new DataOutputStream(soc.getOutputStream());
		
			//ecriture nom fichier
			strOut.writeUTF(FileName);
			
			//réponse serveur
			long size = strIn.readLong();
			//si fichier non présent
			if(size == -1) {
				soc.close();
				System.out.println("Fichier introuvable !");
				System.exit(1);
			}
			
			//creation du fichier destination
			File f = new File(downloads + "/" + FileName);
			f.delete();
			if(!f.createNewFile()) {
				soc.close();
				System.out.println("Création fichier impossible !");
				System.exit(1);
			}
			byte[] buffer = new byte[512];
			FileOutputStream fOut = new FileOutputStream(f);
			System.out.println("Fichier trouvé, début téléchargement !");
			
			//recupération des données :
			long acc = 0;
			int bloc;
			while(acc != size) {
				//lecture de la taille du bloc
				bloc = strIn.readInt();
				System.out.println("Lecture de "+bloc+" octets !");
				
				//lecture du bloc
				strIn.read(buffer, 0, bloc);
				
				//ecriture dans le fichier
				fOut.write(buffer, 0, bloc);
				fOut.flush();
				
				//maj accumulatuer
				acc += bloc;
			}
			Thread.sleep(1000);
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			//soc.close();
			System.out.println("Done !!");
		}
	}
	
	
}
