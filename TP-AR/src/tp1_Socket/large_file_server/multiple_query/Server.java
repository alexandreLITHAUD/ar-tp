package tp1_Socket.large_file_server.multiple_query;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;

public class Server {

	protected static String REPO_PATH;
	protected static int PORT;

	protected static int nbWorker = 5;

	public static void main(String[] args) throws IOException, InterruptedException {

		System.out.println("SERVER STARTING !");

		REPO_PATH = null;
		PORT = 0;

		if (args.length < 2) {
			System.out.println("Missing paramters\n Lanching in default mode\n");
			REPO_PATH = "ressources_serv/";
			PORT = 9999;
			System.out.println("PATH : " + REPO_PATH + "\nPORT : " + PORT + "\n");
		} else {
			System.out.println("Lanching in argument mode\n");
			REPO_PATH = args[0];
			PORT = Integer.valueOf(args[1]);
			System.out.println("PATH : " + REPO_PATH + "\nPORT : " + PORT + "\n");
		}

		ServerSocket sso = new ServerSocket(PORT);

		ArrayBlockingQueue<Socket> clientBuffer = new ArrayBlockingQueue<>(10);

		for (int i = 0; i < nbWorker; i++) {
			Worker w = new Worker(clientBuffer);
			w.setDaemon(true);
			w.start();
		}

		while (true) {

			Socket soc = sso.accept();
			clientBuffer.put(soc);

		}

	}

	public static class Worker extends Thread {

		private Socket soc;
		DataInputStream dis;
		DataOutputStream dos;
		FileInputStream fis = null;

		ArrayBlockingQueue<Socket> clientBuffer;

		public Worker(ArrayBlockingQueue<Socket> pcb) {
			this.clientBuffer = pcb;
		}

		public void run() {

			while (true) {
				
				try {
					this.soc = (Socket)this.clientBuffer.take();
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				try {

					dis = new DataInputStream(soc.getInputStream());
					dos = new DataOutputStream(soc.getOutputStream());

					while (true) {

						String str = dis.readUTF();

						File f = null;

						try {
							f = new File(REPO_PATH + str);
							fis = new FileInputStream(f);
						} catch (FileNotFoundException e) {
							System.err.println("File not found !\n");
							dos.writeLong(-50);
							continue;
						}

						long size = f.length();

						dos.writeLong(size);

						while(size > 512) {
							dos.writeInt(512);
							
							byte[] b = new byte[512];
							fis.read(b, 0, 512);
							
							dos.write(b);
							
							size -= 512;
						}
						
						if(size != 0) {
							dos.writeInt((int)size);
							
							byte[] b = new byte[(int)size];
							fis.read(b, 0, (int)size);
							dos.write(b);
							size = 0;
						}

					}

				} catch (Exception e) {
					System.out.println("WORKER DONE");
				} finally {
					if (dis != null) {
						try {
							dis.close();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					if (dos != null) {
						try {
							dos.close();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					if (fis != null) {
						try {
							fis.close();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					try {
						this.soc.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

	}

}
