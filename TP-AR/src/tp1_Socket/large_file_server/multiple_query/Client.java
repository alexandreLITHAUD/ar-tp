package tp1_Socket.large_file_server.multiple_query;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {

	public static void main(String[] args) throws UnknownHostException, IOException {

		String REPO_PATH = null;
		int PORT = 0;

		if (args.length < 2) {
			System.out.println("Missing paramters\n Lanching in default mode\n");
			REPO_PATH = "ressources_cli/";
			PORT = 9999;
			System.out.println("PATH : " + REPO_PATH + "\nPORT : " + PORT + "\n");
		} else {
			System.out.println("Lanching in argument mode\n");
			REPO_PATH = args[0];
			PORT = Integer.valueOf(args[1]);
			System.out.println("PATH : " + REPO_PATH + "\nPORT : " + PORT + "\n");
		}

		System.out.println("CLIENT STARTING !");

		Socket soc = new Socket("localhost", PORT);

		DataOutputStream dos = null;
		DataInputStream dis = null;
		FileOutputStream fos = null;

		try {

			while (true) {

				dos = new DataOutputStream(soc.getOutputStream());
				dis = new DataInputStream(soc.getInputStream());

				System.out.println("Nom du fichier voulu : (.quit pour quitter)");

				Scanner sc = new Scanner(System.in);

				String file = sc.nextLine();

				if (file.equals(".quit")) {
					break;
				}

				dos.writeUTF(file);

				long length = dis.readLong();

				if (length == -50) {
					System.err.println("File not found !\n");
					continue;
				}

				fos = new FileOutputStream(REPO_PATH + file);

				while (length != 0) {

					int size = dis.readInt();
					byte[] b = new byte[size];
					dis.read(b, 0, size);
					fos.write(b, 0, size);
					length -= size;
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dis != null) {
				try {
					dis.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (dos != null) {
				try {
					dos.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		System.out.println("DONE !\n");

	}

}
