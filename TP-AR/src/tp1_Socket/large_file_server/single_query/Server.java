package tp1_Socket.large_file_server.single_query;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Server {
	public final static int backlog = 3;
	public final static int THREAD_POOL_SIZE = 5;
	public final static int PROD_CONS_SIZE = 10;
	private static final Executor exec = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
	
	public static void main(String[] args) {
		
		if (args.length != 2) {
			System.out.println("Use with 2 arguments : port-number(>1024) and repo-search-name(from the program root)");
			System.exit(1);
		}
		
		int port = Integer.parseInt(args[0]);
		String repo = args[1];
		
		//creation connexion
		ServerSocket listenSoc = null;
		try {
			listenSoc = new ServerSocket(port, backlog);
		}catch(Exception e) {
			System.out.println("Creation socket impossible !");
			e.printStackTrace();
			System.exit(1);
		}
		
		
		System.out.println("Serveur is running ...");
		while(true) {
			Socket soc = null;
			try {
				soc = listenSoc.accept();
				Worker r = new Worker(soc,repo);
				exec.execute(r);
			} catch (IOException e) {
				e.printStackTrace();
			} 	
		}

	}

}
