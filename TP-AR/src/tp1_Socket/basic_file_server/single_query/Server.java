package tp1_Socket.basic_file_server.single_query;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.*;

public class Server {
	public final static int backlog = 3;
	
	public static void main(String[] args) {
		
		if (args.length != 2) {
			System.out.println("Use with 2 arguments : port-number(>1024) and repo-search-name(from the program root)");
			System.exit(1);
		}
		
		int port = Integer.parseInt(args[0]);
		String repo = args[1];
		
		//creation connexion
		ServerSocket listenSoc=null;
		try {
			listenSoc = new ServerSocket(port, backlog);
		}catch(Exception e) {
			System.out.println("Creation socket impossible !");
			e.printStackTrace();
			System.exit(1);
		}
		
		System.out.println("Serveur is running ...");
		while(true) {
			Socket soc=null;
			try {
				soc = listenSoc.accept();
				
				DataInputStream strIn = new DataInputStream(soc.getInputStream());
				DataOutputStream strOut = new DataOutputStream(soc.getOutputStream());
		
				//lecture du nom fichier 
				String fileName = strIn.readUTF();
				
				//verification existance du fichier :
				File f = new File(repo+"/"+fileName);
				if(f.exists() && !f.isDirectory()){
					byte[] buffer = new byte[512];
					long size = f.length();
					
					strOut.writeLong(size);//Code pour fichier trouvé
					
					//gestion du fichier
					FileInputStream fIn = new FileInputStream(f);
					
					//envoie du fichier par morceaux de 512o
					long i=size;
					int offset = 0;
					while(i>0) {
						int numberSend;
						//si plus de 512o restant a envoyer
						if(i>=512) {
							numberSend = 512;
						}//sinon (moins de 512 a envoyer)
						else {
							numberSend = (int) i;
						}
						
						//envoie de la taille du bloc fichier
						strOut.writeInt(numberSend);
						//lecture du bloc dans le fichier
						fIn.read(buffer, 0, numberSend);
						
						//ecriture des données 
						strOut.write(buffer, 0, numberSend);
						
						//maj offset
						offset += numberSend;
						//maj du compteur du restant a envoyer
						i -= numberSend;
					}
					
					//fermeture fichier
					fIn.close();
				}else {
					System.out.println("Fichier non trouvé "+f.getAbsolutePath());
					strOut.writeLong(-1);//Code pour fichier non trouvé
				}
				
				
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				//soc.close();
				System.out.println("Fin de client !");
			}			
			
		}

	}

}
