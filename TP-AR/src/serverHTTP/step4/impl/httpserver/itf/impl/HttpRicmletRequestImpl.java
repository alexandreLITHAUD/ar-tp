package serverHTTP.step4.impl.httpserver.itf.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import serverHTTP.step4.ifaces.httpserver.itf.HttpResponse;
import serverHTTP.step4.ifaces.httpserver.itf.HttpRicmlet;
import serverHTTP.step4.ifaces.httpserver.itf.HttpRicmletRequest;
import serverHTTP.step4.ifaces.httpserver.itf.HttpSession;

public class HttpRicmletRequestImpl extends HttpRicmletRequest{

	public BufferedReader br;
	public Map<String,String> argumentsMap = new HashMap<>();
	public Map<String, String> cookieMap = new HashMap<>();	
	
	public HttpRicmletRequestImpl(HttpServer hs, String method, String ressname, BufferedReader br,String arguments) throws IOException {
		super(hs, method, ressname, br);
		this.br = br;
		parseArguments(arguments);
		parseCookie();
	}
	
	private void parseArguments(String arguments) {
		if(arguments == null) {
			return;
		}
		String[] args = arguments.split("&");
		
		try {
			for(String s : args) {
				argumentsMap.put(s.split(String.valueOf('='))[0], s.split(String.valueOf('='))[1]);
			}
		}catch (Exception e){
			// WRONG ARGUMENT FORM TO BE TAKEN OF
		}
	}
	
	private void parseCookie() throws IOException {
		String cookieLine = br.readLine();
		String[] cookieLineSplit;
		String[] cookieLineSplitArgs;
		
		while(cookieLine != null && !cookieLine.equals("")){ //cookieLine != null car une req peut etre mal formée
			cookieLineSplit = cookieLine.split(":");
			if(cookieLineSplit[0].equals("Cookie")){
				cookieLineSplitArgs = cookieLineSplit[1].split(";");
				for(String cookieTok : cookieLineSplitArgs){
					this.cookieMap.put((cookieTok.split("=")[0]).trim(),(cookieTok.split("=")[1]).trim());
				}
			}
			cookieLine = br.readLine();
		}
	}

	@Override
	public String getArg(String name) {
		if(argumentsMap.containsKey(name)) {
			return argumentsMap.get(name);
		}
		return null;
	}

	@Override
	public String getCookie(String name) {
		if(this.cookieMap.containsKey(name)) {
			return this.cookieMap.get(name);
		}
		return null;
	}

	@Override
	public void process(HttpResponse resp) throws Exception {
		
		HttpRicmlet c = null;
		
		try {
			c = this.m_hs.getInstance(m_ressname);
		}catch(ClassNotFoundException e) {
			resp.setReplyError(404, "Ricmlet Not Found");
			return;
		}
		
		HttpRicmletResponseImpl rim = (HttpRicmletResponseImpl)resp;
		
		rim.setCookie("session-id",getSession().getId());
		
		c.doGet((HttpRicmletRequestImpl)rim.m_req, rim);
	}

	@Override
	public HttpSession getSession() {
		
		String id = this.getCookie("session-id");
		return this.m_hs.getSession(id);		
	}

}
