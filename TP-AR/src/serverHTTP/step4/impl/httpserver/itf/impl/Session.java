package serverHTTP.step4.impl.httpserver.itf.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import serverHTTP.step4.ifaces.httpserver.itf.HttpSession;

public class Session implements HttpSession {

	private final int TIMER_VALUE = 60;
	protected Map<String, Object> sessionMap = new HashMap<>();
	private String id;

	Countdown t;
	
	public Session(String id, HttpServer hs) {
		this.id = id;
		
		this.t = new Countdown(TIMER_VALUE, hs, this);
		t.start();
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public Object getValue(String key) {
		this.t.refreshTimer(); // un acces donc reset le timer
		if (sessionMap.containsKey(key)) {
			return sessionMap.get(key);
		}
		return null;
	}

	@Override
	public void setValue(String key, Object value) {
		this.t.refreshTimer(); // un acces donc reset le timer
		this.sessionMap.put(key, value);
	}

	private static class Countdown extends Thread {

		private int timeInS;
		private HttpServer hs;
		private Session session;
		private int actualValue = 0;

		public Countdown(int time, HttpServer hs, Session session) {
			if (time < 0) {
				time = 60;
			}
			this.timeInS = time;
			this.hs = hs;
			this.session = session;
			this.setDaemon(true);
		}
		
		public void refreshTimer() {
			this.actualValue = 0;
		}

		@Override
		public void run() {

			while(true) {
				
				if(actualValue == timeInS) {
					break;
				}
				
				System.out.println(actualValue + "  /  " + timeInS);
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				actualValue++;
			}
			this.hs.purgeSession(this.session.id);
		}
	}
}
