package serverHTTP.step4.impl.httpserver.itf.impl;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;

import serverHTTP.step4.ifaces.httpserver.itf.HttpRequest;
import serverHTTP.step4.ifaces.httpserver.itf.HttpResponse;
import serverHTTP.step4.ifaces.httpserver.itf.HttpRicmlet;
import serverHTTP.step4.ifaces.httpserver.itf.HttpSession;


/**
 * Basic HTTP Server Implementation 
 * 
 * Only manages static requests
 * The url for a static ressource is of the form: "http//host:port/<path>/<ressource name>"
 * For example, try accessing the following urls from your brower:
 *    http://localhost:<port>/
 *    http://localhost:<port>/voile.jpg
 *    ...
 */
public class HttpServer {

	private int m_port;
	private File m_folder;  // default folder for accessing static resources (files)
	private ServerSocket m_ssoc;
	
	private Map<String, HttpRicmlet> instanceMap = new HashMap<>();
	private Map<String,HttpSession> sessionInstanceMap = new HashMap<>();

	protected HttpServer(int port, String folderName) {
		m_port = port;
		if (!folderName.endsWith(File.separator)) 
			folderName = folderName + File.separator;
		m_folder = new File(folderName);
		try {
			m_ssoc=new ServerSocket(m_port);
			System.out.println("HttpServer started on port " + m_port);
		} catch (IOException e) {
			System.out.println("HttpServer Exception:" + e );
			System.exit(1);
		}
	}
	
	public File getFolder() {
		return m_folder;
	}

	public synchronized HttpRicmlet getInstance(String clsname)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, MalformedURLException, 
			IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		if(instanceMap.containsKey(clsname)) {
			return instanceMap.get(clsname);
		}
		else {
			Class<?> c = Class.forName(clsname);
			instanceMap.put(clsname, (HttpRicmlet)c.getDeclaredConstructor().newInstance());
			return instanceMap.get(clsname);
		}
	}
	
	public synchronized HttpSession getSession(String id) {
		if(id == null) {
			id = UUID.randomUUID().toString();
			this.sessionInstanceMap.put(id, new Session(id,this));
		}
		
		else if(this.sessionInstanceMap.get(id) == null) {
			this.sessionInstanceMap.put(id, new Session(id,this));
		}
		
		return this.sessionInstanceMap.get(id);
	}
	
	public synchronized void purgeSession(String id) {
		this.sessionInstanceMap.remove(id);
	}


	private boolean isDynamicRequest(String ressname){
		return ressname.split("/")[1].equals("ricmlets");
	}


	/*
	 * Reads a request on the given input stream and returns the corresponding HttpRequest object
	 */
	public HttpRequest getRequest(BufferedReader br) throws IOException {
		HttpRequest request = null;
		
		String startline = br.readLine();
		StringTokenizer parseline = new StringTokenizer(startline);
		String method = parseline.nextToken().toUpperCase(); 
		String ressname = parseline.nextToken();
		if (method.equals("GET")) {
			
			if(isDynamicRequest(ressname)) { // Regarde si y a une appel au dossier rimclets
				String classname = ressname.replace('/','.'); // Remplace tous les / par des . pour le nom des instances 
				classname = "serverHTTP.step4" + classname;
				String[] split = classname.split("\\?"); // Regarde si il y a des arguments dans la requete
				if(split.length == 1) {
					request = new HttpRicmletRequestImpl(this, method, classname, br, null);
				}
				else {
					request = new HttpRicmletRequestImpl(this, method, split[0], br, split[1]);
				}
			}
			else { // Requete static
				
				String classname = "FILES/" + ressname; 
				
				request = new HttpStaticRequest(this, method, classname);
			}
			
		} else 
			request = new UnknownRequest(this, method, ressname);
		return request;
	}


	/*
	 * Returns an HttpResponse object associated to the given HttpRequest object
	 */
	public HttpResponse getResponse(HttpRequest req, PrintStream ps) {
		
		if(req instanceof HttpRicmletRequestImpl) { // Si requete dynamique reponse dynamique
			return new HttpRicmletResponseImpl(this, req, ps);
		}
		
		return new HttpResponseImpl(this, req, ps);
	}


	/*
	 * Server main loop
	 */
	protected void loop() {
		try {
			while (true) {
				Socket soc = m_ssoc.accept();
				(new HttpWorker(this, soc)).start();
			}
		} catch (IOException e) {
			System.out.println("HttpServer Exception, skipping request");
			e.printStackTrace();
		}
	}

	
	
	public static void main(String[] args) {
		int port = 0;
		if (args.length != 2) {
			System.out.println("Usage: java Server <port-number> <file folder>");
		} else {
			port = Integer.parseInt(args[0]);
			String foldername = args[1];
			HttpServer hs = new HttpServer(port, foldername);
			hs.loop();
		}
	}

}

