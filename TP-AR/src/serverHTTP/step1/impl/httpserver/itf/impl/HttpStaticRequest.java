package serverHTTP.step1.impl.httpserver.itf.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;

import serverHTTP.step1.ifaces.httpserver.itf.HttpRequest;
import serverHTTP.step1.ifaces.httpserver.itf.HttpResponse;


/*
 * This class allows to build an object representing an HTTP static request
 */
public class HttpStaticRequest extends HttpRequest {
	static final String DEFAULT_FILE = "index.html";
	
	public HttpStaticRequest(HttpServer hs, String method, String ressname) throws IOException {
		super(hs, method, ressname);
	}
	
	public void process(HttpResponse resp) throws Exception {
		
		File f = null;
		if(this.m_ressname.endsWith("/")) {
			this.m_ressname += DEFAULT_FILE;
			f = new File(this.m_hs.getFolder(),this.m_ressname);
		}
		else {
			f = new File(this.m_hs.getFolder(),this.m_ressname);
			
			if(!(f.exists() && f.isFile())) {
				resp.setReplyError(404, "File not found" + f.getAbsolutePath());
				return;
			}
		}

		resp.setReplyOk();
		resp.setContentLength((int)f.length());
		resp.setContentType(this.getContentType(f.getAbsolutePath()));
		PrintStream ps = resp.beginBody();
		
		FileInputStream fis = new FileInputStream(f);
		
		byte[] buffer = new byte[(int)f.length()];
		
		fis.read(buffer);
		
		ps.write(buffer);
	}

}
