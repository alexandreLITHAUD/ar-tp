package tp2_NIO;

import java.nio.channels.SelectionKey;

public class Automatons {
	private WriterAutomata wa;
	private ReaderAutomata ra;
	
	public Automatons(WriterAutomata wa,ReaderAutomata ra) {
		this.wa = wa;
		this.ra = ra;
	}
	
	public Automatons(SelectionKey k,NIOInterface device) {
		this.wa = new WriterAutomata(k,device);
		this.ra = new ReaderAutomata(k,device);
		
	}
	
	public WriterAutomata getWriterAutomata() {
		return wa;
	}
	public ReaderAutomata getReaderAutomata() {
		return ra;
	}
}
