package tp2_NIO;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;

public class WriterAutomata {
	//automates
	private enum State {WRITING_LENGTH, WRITING_MSG,WRITING_IDLE} ; 
	private State state = State.WRITING_IDLE;
	
	//buffers
	private ByteBuffer buffSize = ByteBuffer.allocate(4);
	private ByteBuffer buffMessage;
	private ArrayList<byte[]> pendingMsgs = new ArrayList<>();
	
	//channel
	private SelectionKey key;
	private SocketChannel sc;
	
	private NIOInterface device;
	
	public WriterAutomata(SelectionKey key, NIOInterface device) {
		this.key = key;
		this.device = device;
		sc = (SocketChannel) key.channel();
	}
	
	
	public void handleWrite() throws IOException{
		switch (state) {
		case WRITING_LENGTH:
			sc.write(buffSize);
			if(buffSize.remaining() == 0) {
				state = State.WRITING_MSG;
			}
			break;
		case WRITING_MSG:
			sc.write(buffMessage);
			if(buffMessage.remaining() == 0) {
				state = State.WRITING_IDLE;
			}
			break;
		case WRITING_IDLE:
			if(!pendingMsgs.isEmpty()) { // set buffers :
				byte[] msg = pendingMsgs.get(0);
				pendingMsgs.remove(0);
				
				buffMessage = ByteBuffer.allocate(msg.length);
				buffMessage.put(msg);
				buffMessage.rewind();
				
				buffSize.rewind();
				buffSize.putInt(msg.length);
				buffSize.rewind();
				
				state = State.WRITING_LENGTH;	
			}else {
				// remove the write interest & set READ interest
				key.interestOps(SelectionKey.OP_READ);
			}
			break;
		default :
			System.err.println("Etat inconnu");
			System.exit(1);
		}
	}
	
	public void sendMsg(byte[] msg) {
		pendingMsgs.add(msg);
	}
	
}


