package tp2_NIO;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

public class ReaderAutomata {
	//automates
	private enum State {READING_LENGTH, READING_MSG} ; 
	private State state = State.READING_LENGTH;
	
	//buffers
	private ByteBuffer buffSize = ByteBuffer.allocate(4);
	private ByteBuffer buffMessage;
	private int tailleMsg;
	
	//channel
	private SelectionKey key;
	private SocketChannel sc;
	private NIOInterface device;
	
	public ReaderAutomata(SelectionKey key, NIOInterface device) {
		this.key = key;
		sc = (SocketChannel) key.channel();
		this.device = device;
	}
	
	
	public void handleRead() throws IOException{
		switch (state) {
		case READING_LENGTH:
			sc.read(buffSize);
			if(buffSize.remaining() == 0) {
				buffSize.rewind();
				tailleMsg = buffSize.getInt();
				buffSize.rewind();
				buffMessage = ByteBuffer.allocate(tailleMsg);
				state = State.READING_MSG;
			}
			break;
		case READING_MSG:
			sc.read(buffMessage);
			if(buffMessage.remaining() == 0) {
				byte[] data = new byte[buffMessage.position()];
				buffMessage.rewind();
				buffMessage.get(data);
				buffMessage = null;
				state = State.READING_LENGTH;
				device.traiter_message(data,key);
				
			}
			break;
		default :
			System.err.println("Etat inconnu");
			System.exit(1);
		}
	}
	
}
