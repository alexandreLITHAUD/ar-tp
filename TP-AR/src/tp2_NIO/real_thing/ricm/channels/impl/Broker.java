package tp2_NIO.real_thing.ricm.channels.impl;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.ArrayList;
import java.util.Iterator;

import tp2_NIO.Automatons;
import tp2_NIO.real_thing.ricm.channels.IBroker;
import tp2_NIO.real_thing.ricm.channels.IBrokerListener;
import tp2_NIO.real_thing.ricm.channels.IChannel;

/**
 * Broker implementation
 */

public class Broker implements IBroker {
	private IBrokerListener listener = null;

	// NIO selector
	private Selector selector;
	
	// The server channel to accept connections from clients
	private ServerSocketChannel ssc;
	// The client channel to communicate with the server 
	private SocketChannel sc;
	// The selection key to register events of interests
	private SelectionKey skey;
	
	//use in case of connexion refused
	private String host;
	private int port;

	public Broker() throws Exception {
		// create a selector
		this.selector = SelectorProvider.provider().openSelector();
	}


	@Override
	public void setListener(IBrokerListener l) {
		if(listener == null) {
			listener = l;
		}else {
			System.err.println("Broker deja défini");
			System.exit(2);
		}
		
	}

	@Override
	public boolean connect(String host, int port) { //appeler uniquement par client
		this.host = host;
		this.port = port;
		// create a non-blocking socket channel
		try {
			sc = SocketChannel.open();
			sc.configureBlocking(false);

			// register a CONNECT interest for channel sc 
			skey = sc.register(selector, SelectionKey.OP_CONNECT);
			
			//settings automatons TODO
			//skey.attach(new Automatons(skey,this));

			// request to connect to the server
			InetAddress addr;
			addr = InetAddress.getByName(host);
			sc.connect(new InetSocketAddress(addr, port));
		} catch (IOException e) {
			//e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean accept(int port) { // appeler uniquement par serveur
		this.host = "localhost";
		this.port = port;
		try {
			// create a new selector
			this.selector = SelectorProvider.provider().openSelector();
			// create a new non-blocking server socket channel
			this.ssc = ServerSocketChannel.open();
			ssc.configureBlocking(false);
			
			// bind the server socket to the given address and port
			InetAddress hostAddress;
			hostAddress = InetAddress.getByName(host);
			InetSocketAddress isa = new InetSocketAddress(hostAddress, port);
			ssc.socket().bind(isa);

			// register a ACCEPT interest for channel ssc
			skey = ssc.register(selector, SelectionKey.OP_ACCEPT);
			
		} catch (IOException e) { //echec de connexion
			//e.printStackTrace();
			return false;
		}
		return true;
	}


	public void run() throws IOException {
		System.out.println("Engine Serveur running");
		while (true) {
			// wait for some events
			selector.select();

			// get the keys for which the events occurred
			Iterator<?> selectedKeys = this.selector.selectedKeys().iterator();

			while (selectedKeys.hasNext()) {

				SelectionKey key = (SelectionKey) selectedKeys.next();
				selectedKeys.remove();
				
				// process the event
				if (key.isValid() && key.isAcceptable())  // accept event
					handleAccept(key);
				if (key.isValid() && key.isReadable())    // read event
					((Channel) key.attachment()).reader.handleRead();
				if (key.isValid() && key.isWritable())    // write event
					((Channel) key.attachment()).writer.handleWrite();
				if (key.isValid() && key.isConnectable())  // connect event
					handleConnect(key);
			}
		}
		
	}

	private void handleAccept(SelectionKey key) {
		assert (this.skey == key);
		assert (ssc == key.channel());
		SocketChannel sc;

		// do the actual accept on the server-socket channel
		// get a client channel as result
		try {
			sc = ssc.accept();
			sc.configureBlocking(false);
			
			// register a READ interest on sc to receive the message sent by the client
			SelectionKey k = sc.register(selector, SelectionKey.OP_READ);
			
			//attach the channel
			IChannel ch = new Channel(k);
			k.attach(ch);
			
			//callback
			listener.accepted(ch);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			listener.refused(host, port);
		}
		
	}


	private void handleConnect(SelectionKey key) { //TODO Si on attrape une exception, refused
		assert (this.skey == key);
		assert (sc == key.channel());

		try {
			if(!sc.finishConnect()) { 
				listener.refused(host, port);
				return;
			}
			skey.interestOps(SelectionKey.OP_READ);
			
			//attach the channel to the key
			IChannel ch = new Channel(skey);
			skey.attach(ch);
			
			//callback
			listener.connected(ch);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			listener.refused(host, port);
		}
		
		//skey.interestOps(SelectionKey.OP_WRITE);
	}
}
