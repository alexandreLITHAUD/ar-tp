package tp2_NIO.real_thing.ricm.channels.impl;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;


public class Reader {

	//automates
	private enum State {READING_LENGTH, READING_MSG} ; 
	private State state = State.READING_LENGTH;
	
	//buffers
	private ByteBuffer buffSize = ByteBuffer.allocate(4);
	private ByteBuffer buffMessage;
	private int tailleMsg;
	
	//channel
	private SelectionKey key;
	private SocketChannel sc;
	private Channel channel;
	
	public Reader (SelectionKey key, Channel c) {
		this.key = key;
		this.channel = c;
		sc = (SocketChannel) key.channel();
	}
	
	
	public void handleRead() throws IOException{
		switch (state) {
		case READING_LENGTH:
			sc.read(buffSize);
			if(buffSize.remaining() == 0) {
				buffSize.rewind();
				tailleMsg = buffSize.getInt();
				buffSize.rewind();
				buffMessage = ByteBuffer.allocate(tailleMsg);
				state = State.READING_MSG;
			}
			break;
		case READING_MSG:
			sc.read(buffMessage);
			if(buffMessage.remaining() == 0) {
				byte[] data = new byte[buffMessage.position()];
				buffMessage.rewind();
				buffMessage.get(data);
				buffMessage = null;
				state = State.READING_LENGTH;
				//TODO znvoyer message
				channel.getListener().received(channel, data);
			}
			break;
		default :
			System.err.println("Etat inconnu");
			System.exit(1);
		}
	}
}

