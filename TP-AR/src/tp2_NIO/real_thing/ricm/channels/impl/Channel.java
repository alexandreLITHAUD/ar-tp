package tp2_NIO.real_thing.ricm.channels.impl;

import java.nio.channels.SelectionKey;

import tp2_NIO.real_thing.ricm.channels.IBrokerListener;
import tp2_NIO.real_thing.ricm.channels.IChannel;
import tp2_NIO.real_thing.ricm.channels.IChannelListener;

public class Channel implements IChannel {
	private IChannelListener listener = null;
	public Reader reader; 
	public Writer writer; 
	
	
	public Channel(SelectionKey k) {
		reader = new Reader(k,this);
		writer = new Writer(k,this);
	}

	@Override
	public void setListener(IChannelListener l) {
		if(listener == null) {
			listener = l;
		}else {
			System.err.println("Channel deja défini");
			System.exit(2);
		}
	}
	
	public IChannelListener getListener() {
		return listener;
	}

	@Override
	public void send(byte[] bytes, int offset, int count) {
		byte[] toSend = new byte[count];
		int indice = 0;
		for (int i=offset;i<count;i++) {
			toSend[indice] = bytes[i];
			indice ++;
		}
		writer.sendMsg(toSend);
	}

	@Override
	public void send(byte[] bytes) {
		writer.sendMsg(bytes);
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean closed() {
		// TODO Auto-generated method stub
		return false;
	}
	
	// to complete
}
