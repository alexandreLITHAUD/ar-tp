package tp2_NIO.real_thing.ricm.channels.fileserver.threaded;

import tp2_NIO.real_thing.ricm.channels.IChannel;

public class Request {
	IChannel channel;
	byte[] bytes;
	Request(IChannel channel,byte[] bytes) {
		this.channel = channel;
		this.bytes = bytes;
	}
}