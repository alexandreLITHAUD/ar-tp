#!/bin/bash

NBClient=5
ARGSERV = "9999 ressources_serv"
ARGCLI = "localhost 9999 info.txt"


if [ $# -eq 0 ]
then
	echo "Launching in basic mode NB Client = 10"
else
	echo "Launching in argument mode NB Client = $1"
	NBClient = $1
fi

# CLASSPATH (TO CHANGE FOR YOUR CONFIGURATION)
CLASSPATH = "./TP-AR/bin"

# ======================================================================================= #

:'

SECONDS=0
# Lancer Server Mono Thread
java -classpath $CLASSPATH "tp1_Socket.baby_step.Server" & 
TEMP = $!

# Lancer les NBClient nombre de client
for i in {0..NBClient}
do

# Launch Java + &
java -classpath $CLASSPATH "tp1_Socket.baby_step.Client" &
pids[${i}]=$!

done

for pid in ${pids[*]}; do
    wait $pid
done

kill $TEMP

echo "Elapsed Time for Mono Threaded Server: $SECONDS seconds"

'

# ======================================================================================= #


SECONDS=0
# Lancer Server Multi Thread basic
java -classpath $CLASSPATH "tp1_Socket.basic_file_server.single_query.Server" ARGSERV & 
TEMP = $!

# Lancer les NBClient nombre de client
for i in {0..NBClient}
do

# Launch Java + &
java -classpath $CLASSPATH "tp1_Socket.basic_file_server.single_query.Client" ARGCLI & 
pids[${i}]=$!

done

for pid in ${pids[*]}; do
    wait $pid
done

kill $TEMP

echo "Elapsed Time for Multi Thread Basic Server: $SECONDS seconds"


# ======================================================================================= #


SECONDS=0
# Lancer Server Multi Thread pool based
java -classpath $CLASSPATH "tp1_Socket.large_file_server.single_query.Server" ARGSERV & 
TEMP = $!

# Lancer les NBClient nombre de client
for i in {0..NBClient}
do

# Launch Java + &
java -classpath $CLASSPATH "tp1_Socket.large_file_server.single_query.Client" ARGCLI & 
pids[${i}]=$!

done

for pid in ${pids[*]}; do
    wait $pid
done

kill $TEMP

echo "Elapsed Time for Multi Thread Pool Based Server: $SECONDS seconds"


# ======================================================================================= #

